extern crate primal;

use primal::is_prime;

fn f(n: &u64) -> bool {
    for i in 1..(((n + 1) as f64).sqrt().ceil() as u64) {
        if n % i == 0 {
            if !is_prime(i + n / i) {
                return false;
            }
        }
    }
    true
}

fn main() {
    let res: u64 = (1..100_000_000).filter(f).sum();
    println!("{}", res);
}
